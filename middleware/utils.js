import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

const SECRET_KEY = process.env.JWT_KEY;

/*
 * @params {jwtToken} extracted from cookies
 * @return {object} object of extracted token
 */
export function verifyToken(jwtToken) {
  try {
    return jwt.verify(jwtToken, SECRET_KEY);
    // return true
  } catch (e) {
    console.log('e:', e);
    return null;
  }
}

/*
 * @params {request} extracted from request response
 * @return {object} object of parse jwt cookie decode object
 */
export function getAppCookies(req) {
  const parsedItems = {};
  if (req.headers.cookie) {
    const cookiesItems = req.headers.cookie.split('; ');
    cookiesItems.forEach(cookies => {
      const parsedItem = cookies.split('=');
      parsedItems[parsedItem[0]] = decodeURI(parsedItem[1]);
    });
  }
  return parsedItems;
}

/*
 * @params {request} extracted from request response, {setLocalhost} your localhost address
 * @return {object} objects of protocol, host and origin
 */
export function absoluteUrl(req, setLocalhost) {
  let protocol = 'https:';
  let host = req
    ? req.headers['x-forwarded-host'] || req.headers.host
    : window.location.host;
  if (host.indexOf('localhost') > -1) {
    if (setLocalhost) host = setLocalhost;
    protocol = 'http:';
  }
  return {
    protocol,
    host,
    origin: `${protocol}//${host}`,
    url: req,
  };
}

export function validationField(states, e) {
  const input = (e && e.target.name) || '';
  const errors = {};
  errors[input] = '';

  if (input) {
    if (states[input].required) {
      if (!states[input].value) {
        errors[input] = {
          hint: `${states[e.target.name].label} required`,
          isInvalid: true,
        };
      }
    }
    if (states[input].value && states[input].min > states[input].value.length) {
      errors[input] = {
        hint: `${states[input].label} min ${states[input].min} characters`,
        isInvalid: true,
      };
    }
    if (states[input].value && states[input].max < states[input].value.length) {
      errors[input] = {
        hint: `${states[input].label} max ${states[input].max} characters`,
        isInvalid: true,
      };
    }
    if (
      states[input].validator !== null &&
      typeof states[input].validator === 'object'
    ) {
      if (
        states[input].value &&
        !states[input].validator.regEx.test(states[input].value)
      ) {
        errors[input] = {
          hint: states[input].validator.error,
          isInvalid: true,
        };
      }
    }
  }
  return errors;
}
