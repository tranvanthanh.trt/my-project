// eslint-disable-next-line import/prefer-default-export
export const USER_NAME_INVALID = 'Invalid username pattern';
export const EMAIL_INVALID = 'Invalid email pattern';
export const PASSWORD_INVALID = 'Invalid password pattern';
