import React, { useEffect } from 'react';
/* utils */
import { absoluteUrl } from '../middleware/utils';
import Router from 'next/router';

/* components */
import Layout from '../components/layout/Layout';

export default function Home(props) {
  const { user, origin } = props;

  useEffect(() => {
    if (!user) {
      Router.push('/login');
    }
  }, []);

  return (
    <Layout title="Homepage" url={origin} origin={origin}>
      <div className="container">
        <main>
          <h1 className="title">Welcome to my Website</h1>
          <p className="description">
            <img
              src="/sequelize.svg"
              alt="Sequelize"
              height="120"
              style={{ marginRight: '1rem' }}
            />
          </p>
        </main>
      </div>
    </Layout>
  );
}
/* getServerSideProps */
export async function getServerSideProps(context) {
  const { req } = context;
  const { origin } = absoluteUrl(req);
  return {
    props: {
      origin,
    },
  };
}
