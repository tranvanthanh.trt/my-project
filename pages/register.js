import React, { useEffect, useState } from 'react';

/* utils */
import { absoluteUrl, validationField } from '../middleware/utils';

/* components */
import FormRegister from '../components/form/FormRegister';
import {
  EMAIL_REGEX,
  PASSWORD_REGEX,
  USER_NAME_REGEX,
} from '../constant/regex';
import {
  EMAIL_INVALID,
  PASSWORD_INVALID,
  USER_NAME_INVALID,
} from '../constant/message';
import Layout from '../components/layout/Layout';

/* register schemas */
const FORM_DATA_REGISTER = {
  username: {
    value: '',
    label: 'Username',
    min: 6,
    max: 36,
    required: true,
    validator: {
      regEx: USER_NAME_REGEX,
      error: USER_NAME_INVALID,
    },
  },
  email: {
    value: '',
    label: 'Email',
    min: 10,
    max: 36,
    required: true,
    validator: {
      regEx: EMAIL_REGEX,
      error: EMAIL_INVALID,
    },
  },
  password: {
    value: '',
    label: 'Password',
    min: 6,
    max: 36,
    required: true,
    validator: {
      regEx: PASSWORD_REGEX,
      error: PASSWORD_INVALID,
    },
  },
};

function Register(props) {
  const { baseApiUrl, origin } = props;
  const [loading, setLoading] = useState(false);

  const [stateFormData, setStateFormData] = useState(FORM_DATA_REGISTER);
  const [stateFormError, setStateFormError] = useState({
    email: true,
    password: true,
    username: true,
  });
  // eslint-disable-next-line no-unused-vars
  const [stateFormValid, setStateFormValid] = useState(false);
  const [stateFormMessage, setStateFormMessage] = useState({});

  function onChangeHandler(e) {
    const { name, value } = e.currentTarget;
    const newFormState = {
      ...stateFormData,
      [name]: {
        ...stateFormData[name],
        value,
      },
    };
    setStateFormData(newFormState);

    /* validation handler */
    const errors = validationField(newFormState, e);
    setStateFormError({
      ...stateFormError,
      ...errors,
    });
  }

  async function onSubmitHandler(e) {
    e.preventDefault();

    const data = {
      username: stateFormData.username.value || '',
      email: stateFormData.email.value || '',
      password: stateFormData.password.value || '',
    };

    if (stateFormValid) {
      // Call an external API endpoint to get posts.
      // You can use any data fetching library
      setLoading(!loading);
      const loginApi = await fetch(`${baseApiUrl}/user`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).catch(error => {
        console.error('Error:', error);
      });
      const result = await loginApi.json();
      if (result.status === 'success' && result.message === 'done') {
        window.location.href = '/';
      } else {
        setStateFormMessage(result);
      }
      setLoading(false);
    }
  }

  useEffect(() => {
    setStateFormValid(
      !stateFormError.password &&
        !stateFormError.email &&
        !stateFormError.username,
    );
  }, [stateFormError]);

  return (
    <Layout
      title="Register"
      url={origin}
      origin={origin}
      useHeader={false}
      useFooter={false}
    >
      <div className="container">
        <main className="content-detail">
          <FormRegister
            props={{
              onSubmitHandler,
              onChangeHandler,
              loading,
              stateFormData,
              stateFormError,
              stateFormMessage,
            }}
          />
        </main>
      </div>
    </Layout>
  );
}

/* getServerSideProps */
export async function getServerSideProps(context) {
  const { req } = context;
  const { origin } = absoluteUrl(req);

  const referer = req.headers.referer || '';
  const baseApiUrl = `${origin}/api`;

  return {
    props: {
      origin,
      baseApiUrl,
      referer,
    },
  };
}

export default Register;
