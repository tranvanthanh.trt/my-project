import React, { useEffect, useState } from 'react';
import Router from 'next/router';
import Cookies from 'js-cookie';

/* utils */
import { absoluteUrl, validationField } from '../middleware/utils';

/* components */
import FormLogin from '../components/form/FormLogin';
import { PASSWORD_REGEX, USER_NAME_REGEX } from '../constant/regex';
import { PASSWORD_INVALID, USER_NAME_INVALID } from '../constant/message';
import Layout from '../components/layout/Layout';

// eslint-disable-next-line import/named

/* login schemas */
const FORM_DATA_LOGIN = {
  username: {
    value: '',
    label: 'Username',
    min: 6,
    max: 36,
    required: true,
    validator: {
      regEx: USER_NAME_REGEX,
      error: USER_NAME_INVALID,
    },
  },
  password: {
    value: '',
    label: 'Password',
    min: 6,
    max: 36,
    required: true,
    validator: {
      regEx: PASSWORD_REGEX,
      error: PASSWORD_INVALID,
    },
  },
};

function Login(props) {
  const { baseApiUrl, origin } = props;
  const [loading, setLoading] = useState(false);

  const [stateFormData, setStateFormData] = useState(FORM_DATA_LOGIN);
  const [stateFormError, setStateFormError] = useState({
    username: true,
    password: true,
  });
  // eslint-disable-next-line no-unused-vars
  const [stateFormValid, setStateFormValid] = useState(false);
  const [stateFormMessage, setStateFormMessage] = useState({});

  function onChangeHandler(e) {
    const { name, value } = e.currentTarget;
    const newFormState = {
      ...stateFormData,
      [name]: {
        ...stateFormData[name],
        value,
      },
    };
    setStateFormData(newFormState);

    /* validation handler */
    const errors = validationField(newFormState, e);
    setStateFormError({
      ...stateFormError,
      ...errors,
    });
  }

  async function onSubmitHandler(e) {
    e.preventDefault();

    let data = { ...stateFormData };

    data = {
      ...stateFormData,
      username: stateFormData.username.value || '',
      password: stateFormData.password.value || '',
    };

    if (stateFormValid) {
      // Call an external API endpoint to get posts.
      // You can use any data fetching library
      setLoading(!loading);
      const loginApi = await fetch(`${baseApiUrl}/auth`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).catch(error => {
        console.error('Error:', error);
      });
      const result = await loginApi.json();
      if (result.success && result.token) {
        Cookies.set('token', result.token);
        Router.push('/');
      } else {
        setStateFormMessage(result);
      }
      setLoading(false);
    }
  }

  useEffect(() => {
    setStateFormValid(!stateFormError.password && !stateFormError.username);
  }, [stateFormError]);

  return (
    <Layout
      title="Login"
      url={origin}
      origin={origin}
      useHeader={false}
      useFooter={false}
    >
      <div className="container">
        <main className="content-detail">
          <FormLogin
            props={{
              onSubmitHandler,
              onChangeHandler,
              loading,
              stateFormData,
              stateFormError,
              stateFormMessage,
            }}
          />
        </main>
      </div>
    </Layout>
  );
}

/* getServerSideProps */
export async function getServerSideProps(context) {
  const { req } = context;
  const { origin } = absoluteUrl(req);

  const referer = req.headers.referer || '';
  const baseApiUrl = `${origin}/api`;

  return {
    props: {
      origin,
      referer,
      baseApiUrl,
    },
  };
}

export default Login;
