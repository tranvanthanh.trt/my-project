import nextConnect from 'next-connect';
import middleware from '../../../middleware/auth';

const models = require('../../../db/models/index');

const handler = nextConnect()
  // Middleware
  .use(middleware)
  // Get method
  .get(async (req, res) => {
    res.end('method - get');
  })
  // Post method
  .post(async (req, res) => {
    const { body } = req;
    const { username, email, password } = body;
    /* Check user in database */
    const user = await models.users.findOne({
      where: { username },
      limit: 1,
    });
    if (user) {
      return res
        .status(400)
        .json({ status: 'error', error: 'User already exist' });
    }
    const newUser = await models.users.create({
      username,
      email,
      password,
      status: 1,
    });
    return res.status(200).json({
      status: 'success',
      message: 'done',
      data: newUser,
    });
  })
  // Put method
  .put(async (req, res) => {
    res.end('method - put');
  })
  // Patch method
  // eslint-disable-next-line no-unused-vars
  .patch(async (req, res) => {
    throw new Error('Throws me around! Error can be caught and handled.');
  });

export default handler;
