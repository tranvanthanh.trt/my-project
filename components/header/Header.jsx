import React from 'react';
import Link from 'next/link';
import Cookies from 'js-cookie';

const Header = () => {
  const logout = () => {
    Cookies.remove('token');
    window.location.href = '/login';
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg p-2">
        <Link href="/">
          <a className="nav-item nav-link">Home</a>
        </Link>
        <button className="btn-warning" type="button" onClick={logout}>
          Sign out
        </button>
      </nav>
      <style jsx>{`
        a {
          margin: 0 10px 0 0;
        }
      `}</style>
    </>
  );
};

export default Header;
