import React from 'react';

export default function Footer() {
  return (
    <footer className="footer-main text-center">
      <div className="d-block">
        <small>Copyright 2023</small>
      </div>
    </footer>
  );
}
