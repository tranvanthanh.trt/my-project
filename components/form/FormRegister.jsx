/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';

function FormRegister({ props }) {
  const {
    onSubmitHandler,
    onChangeHandler,
    loading,
    stateFormData,
    stateFormError,
    stateFormMessage,
  } = props;

  return (
    <form
      onSubmit={onSubmitHandler}
      className="form-register card"
      method="POST"
    >
      <div className="form-group">
        <h2 className="text-center">Register</h2>
        <h4 className="warning text-center">{stateFormMessage.error}</h4>
      </div>
      <div className="form-group">
        <label htmlFor="email">Username</label>
        <input
          onChange={onChangeHandler}
          className="form-control"
          type="text"
          id="username"
          name="username"
          placeholder="Username"
          readOnly={loading && true}
          value={stateFormData.username.value}
        />
        <p className="warning">{stateFormError.username.hint}</p>
      </div>
      <div className="form-group">
        <label htmlFor="email">Email</label>
        <input
          onChange={onChangeHandler}
          className="form-control"
          type="text"
          id="email"
          name="email"
          placeholder="Email"
          readOnly={loading && true}
          defaultValue={stateFormData.email.value}
        />
        <p className="warning">{stateFormError.email.hint}</p>
      </div>
      <div className="form-group">
        <label htmlFor="password">Password</label>
        <input
          onChange={onChangeHandler}
          className="form-control"
          type="password"
          id="password"
          name="password"
          placeholder="Password"
          readOnly={loading && true}
          defaultValue={stateFormData.password.value}
        />
        <p className="warning">{stateFormError.password.hint}</p>
      </div>
      <div>
        <button
          type="submit"
          className="btn btn-block btn-warning"
          disabled={
            loading ||
            stateFormError.username ||
            stateFormError.password ||
            stateFormError.email
          }
        >
          {!loading ? 'Register' : 'Registering...'}
        </button>
      </div>
      <div className="text-center">or</div>
      <div className="text-center">
        <a href="/login">Sign in</a>
      </div>
    </form>
  );
}
export default FormRegister;
