/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';

function FormLogin({ props }) {
  const {
    onSubmitHandler,
    onChangeHandler,
    loading,
    stateFormData,
    stateFormError,
    stateFormMessage,
  } = props;
  return (
    <form className="form-login card" method="POST" onSubmit={onSubmitHandler}>
      <div className="form-group">
        <h2 className="text-center">Sign in</h2>
        <h4 className="warning text-center">{stateFormMessage.error}</h4>
      </div>
      <div className="form-group">
        <label htmlFor="username">Username</label>
        <input
          className="form-control"
          type="text"
          id="username"
          name="username"
          placeholder="Username"
          onChange={onChangeHandler}
          readOnly={loading && true}
          value={stateFormData.username.value}
        />
        <p className="warning">{stateFormError.username.hint}</p>
      </div>
      <div className="form-group">
        <label htmlFor="password">Password</label>
        <input
          className="form-control"
          type="password"
          id="password"
          name="password"
          placeholder="Password"
          onChange={onChangeHandler}
          readOnly={loading && true}
          value={stateFormData.password.value}
        />
        <p className="warning">{stateFormError.password.hint}</p>
      </div>
      <div>
        <button
          type="submit"
          className="btn btn-block btn-warning"
          disabled={
            loading || stateFormError.username || stateFormError.password
          }
        >
          {!loading ? 'Sign in' : 'Loading...'}
        </button>
      </div>
      <div>
        <span>Don&apos;t have an account yet?</span>&nbsp;
        <a href="/register">Register now</a>
      </div>
    </form>
  );
}
export default FormLogin;
