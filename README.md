# Environment
  - NodeJs : v12
  - Database: MySQL v8
## Installation

Clone the repo:

```bash
git clone https://gitlab.com/tranvanthanh.trt/my-project.git
cd my-project
```

Install the dependencies:

```bash
yarn install
```
Create MySQL database in your local server:

```bash
CREATE DATABASE my_project;
```

Set the environment variables:

```bash
cp .env.example .env

# open .env and modify the environment variables (if needed)
```
### Start the Next.js dev server and open up http://localhost:3000/

```bash
yarn dev
```

==================================================================================
